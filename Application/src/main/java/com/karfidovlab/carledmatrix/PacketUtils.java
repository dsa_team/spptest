package com.karfidovlab.carledmatrix;

import java.io.UnsupportedEncodingException;

public class PacketUtils {

    public enum BLEValues {
        BLEVAL_FW_VER, // R, string
        BLEVAL_HW_VER,// R, string

        BLEVAL_FG_COLOR,// RW, NVS, u32
        BLEVAL_BG_COLOR,// RW, NVS, u32
        BLEVAL_BRIGHTNESS,// RW, NVS, u16
        BLEVAL_GAMMA,// RW, NVS, u16
        BLEVAL_MODE,// RW, NVS, u8
        BLEVAL_SCROLLSPEED,// RW, NVS, u16
        BLEVAL_FREQ,// RW, NVS, u32
        BLEVAL_BITPLANES,// RW, NVS, u8
        BLEVAL_FILENAME,// RW, string
        BLEVAL_DEFAULTFILE,// RW, NVS, string
        BLEVAL_DEVICENAME,// RW, NVS, string
        BLEVAL_SCROLLTEXT,// RW, NVS, string
        BLEVAL_SCROLLFONT,// RW, NVS, string

        BLEVAL_FOP_OPENFILE,// RW, string
        BLEVAL_FOP_REMOVEFILE,// RW, string
        BLEVAL_FOP_DATA,// RW, bytes
        BLEVAL_FOP_POS,// RW, u32
        BLEVAL_FOP_HASH,// R, 16 bytes
        BLEVAL_FILE_COUNT,// R, u32
        BLEVAL_FILE_INDEX,// RW, u32
        BLEVAL_FILE_NAME,// R, string
        BLEVAL_OTA_WRITE,// W, bytes
        BLEVAL_FREE_HEAP,// R, u32
        BLEVAL_CHIP_ID,// R, 6 bytes
        BLEVAL_CMD,// W, u32
        BLEVAL_FOP_REWRITEFILE,// RW, string
        BLEVAL_FOP_CLEANSIZE,// RW, u32
        BLEVAL_FILE_SIZE, // R, u32
    }

    public enum BLETRFCmd {
        BLETRF_CMD_READ,
        BLETRF_CMD_WRITE,
        BLETRF_CMD_NOTIFY
    }

    public enum BLETRFResult {
        BLETRF_RESULT_OK,
        BLETRF_RESULT_HAS_DATA,
        BLETRF_RESULT_FAIL
    }

    public enum DisplayMode {
        DISPMODE_ANIMATION,
        DISPMODE_PLAY_TEMPORARY,
        DISPMODE_PLAY_ALL,
        DISPMODE_PLAY_SINGLE,
        DISPMODE_TEXT,
        DISPMODE_FGFILL,
        DISPMODE_GHOSTTEST,
        DISPMODES_LEN
    }

    public enum BLECmd {
        BLECMD_BEGIN_OTA,
        BLECMD_END_OTA,
        BLECMD_RESTART,
        BLECMD_NVS_STORE,
        BLECMD_NVS_LOAD,
        BLECMDS_LEN
    }

    public static byte[] constructPacket(BLETRFCmd cmd, BLEValues bleval, byte[] payload) {
        // Allocate memory for packet
        int payLen = payload.length;
        byte[] packet = new byte[payLen + 5];

        // Calculate checksum
        int checksum = 0xEF;
        for (byte b : payload) {
            checksum ^= (b & 0xFF);
        }

        // Construct packet
        packet[0] = (byte) checksum;
        packet[1] = (byte) cmd.ordinal();
        packet[2] = (byte) bleval.ordinal();
        packet[3] = (byte) (payLen & 0xFF);
        packet[4] = (byte) ((payLen >> 8) & 0xFF);

        // Copy payload to packet
        System.arraycopy(payload, 0, packet, 5, payLen);

        return packet;
    }

    public static byte[] constructWriteStringPacket(BLEValues bleval, String str) {
        try {
            byte[] payload = str.getBytes("UTF-8");
            return PacketUtils.constructPacket(
                    PacketUtils.BLETRFCmd.BLETRF_CMD_WRITE,
                    bleval,
                    payload);
        } catch (UnsupportedEncodingException e) {
        }
        return null;
    }

    public static byte[] construstWriteIntPacket(BLEValues bleval, int val) {
        byte[] payload = new byte[4];
        payload[0] = (byte) (val & 0xFF);
        payload[1] = (byte) ((val >> 8) & 0xFF);
        payload[2] = (byte) ((val >> 16) & 0xFF);
        payload[3] = (byte) ((val >> 24) & 0xFF);
        return PacketUtils.constructPacket(
                PacketUtils.BLETRFCmd.BLETRF_CMD_WRITE,
                bleval,
                payload);
    }

    public static byte[] construstReadPacket(BLEValues bleval) {
        return PacketUtils.constructPacket(
                PacketUtils.BLETRFCmd.BLETRF_CMD_READ,
                bleval,
                new byte[]{});
    }

    public static int intFromByteArray(byte[] arr) {
        int data32 = 0;
        int len = arr.length;
        if (len >= 4)
            data32 |= (arr[3] & 0xFF) << 24;
        if (len >= 3)
            data32 |= (arr[2] & 0xFF) << 16;
        if (len >= 2)
            data32 |= (arr[1] & 0xFF) << 8;
        if (len >= 1)
            data32 |= (arr[0] & 0xFF);
        return data32;
    }

}
