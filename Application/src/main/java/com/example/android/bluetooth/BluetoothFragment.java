/*
 * Copyright (C) 2014 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.example.android.bluetooth;

import android.app.Activity;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.Intent;
import android.content.res.AssetFileDescriptor;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayout;
import android.support.v7.widget.PopupMenu;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import com.example.android.common.logger.Log;
import com.felipecsl.gifimageview.library.GifImageView;
import com.karfidovlab.carledmatrix.PacketUtils;

import org.apache.commons.io.FileUtils;

import java.io.ByteArrayOutputStream;
import java.io.DataInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.util.Random;

import static android.app.Activity.RESULT_OK;
import static com.karfidovlab.carledmatrix.PacketUtils.BLETRFResult.BLETRF_RESULT_HAS_DATA;

/**
 * This fragment controls Bluetooth to communicate with other devices.
 */
public class BluetoothFragment extends Fragment {

    private static final String TAG = "BluetoothFragment";

    // Intent request codes
    private static final int REQUEST_CONNECT_DEVICE_SECURE = 1;
    private static final int REQUEST_CONNECT_DEVICE_INSECURE = 2;
    private static final int REQUEST_ENABLE_BT = 3;

    private static final int IMAGE_COUNT = 9;
    private static final int ACTIVITY_CHOOSE_FILE = 7;

    // Layout Views
    //private ListView mConversationView;
    private Button mSendButton;
    private TextView mFWTextView;
    private SeekBar mBrightnessSeekBar;
    private SeekBar mGammaSeekBar;
    private Button mModeLogoButton;
    private Button mModeAnimButton;
    private Button mModeFillButton;
    private LinearLayout mGifsList;
    private Button mUploadAllButton;
    private ProgressBar mProgressBar;
    private GridLayout mGifGrid;
    private Button mUploadFWButton;

    private GifImageView[] mGifGridItems;

    private int mGridPlacerCounter;
    /**
     * Name of the connected device
     */
    private String mConnectedDeviceName = null;

    /**
     * Local Bluetooth adapter
     */
    private BluetoothAdapter mBluetoothAdapter = null;

    /**
     * Member object for the chat services
     */
    private BluetoothService mCmdService = null;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
        // Get local Bluetooth adapter
        mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();

        // If the adapter is null, then Bluetooth is not supported
        if (mBluetoothAdapter == null) {
            FragmentActivity activity = getActivity();
            Toast.makeText(activity, "Bluetooth is not available", Toast.LENGTH_LONG).show();
            activity.finish();
        }
    }


    @Override
    public void onStart() {
        super.onStart();
        // If BT is not on, request that it be enabled.
        // setupChat() will then be called during onActivityResult
        if (!mBluetoothAdapter.isEnabled()) {
            Intent enableIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
            startActivityForResult(enableIntent, REQUEST_ENABLE_BT);
            // Otherwise, setup the chat session
        } else if (mCmdService == null) {
            setupChat();
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (mCmdService != null) {
            mCmdService.stop();
        }
    }

    @Override
    public void onResume() {
        super.onResume();

        // Performing this check in onResume() covers the case in which BT was
        // not enabled during onStart(), so we were paused to enable it...
        // onResume() will be called when ACTION_REQUEST_ENABLE activity returns.
        if (mCmdService != null) {
            // Only if the state is STATE_NONE, do we know that we haven't started already
            if (mCmdService.getState() == BluetoothService.STATE_NONE) {
                // Start the Bluetooth chat services
                mCmdService.start();
            }
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_bluetooth_chat, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        mFWTextView = view.findViewById(R.id.fw_ver_text);
        mBrightnessSeekBar = view.findViewById(R.id.brightness_seek_bar);
        mGammaSeekBar = view.findViewById(R.id.gamma_seek_bar);
        mModeLogoButton = view.findViewById(R.id.mode_logo_btn);
        mModeAnimButton = view.findViewById(R.id.mode_anim_btn);
        mModeFillButton = view.findViewById(R.id.mode_fill_btn);
        mGifsList = view.findViewById(R.id.gifs_list);
        mUploadAllButton = view.findViewById(R.id.upload_all);
        mProgressBar = view.findViewById(R.id.progress_bar);
        mGifGrid = view.findViewById(R.id.gifs_grid);
        mUploadFWButton = view.findViewById(R.id.upload_fw_btn);

        mGifGridItems = new GifImageView[IMAGE_COUNT];
        mGridPlacerCounter = 0;
    }

    public void queueGifUpload(String remoteName, byte[] data, BluetoothService.Cmd.Callback cleanCallback,
                               BluetoothService.Cmd.Callback progressCallback) {
        int bufferSize = 7936;

        queueCmd(PacketUtils.construstWriteIntPacket(
                PacketUtils.BLEValues.BLEVAL_FOP_CLEANSIZE,
                data.length), cleanCallback);
        queueCmd(PacketUtils.constructWriteStringPacket(
                PacketUtils.BLEValues.BLEVAL_FOP_REWRITEFILE,
                remoteName));

        byte[] buffer = new byte[bufferSize];
        for (int pos = 0; pos < data.length; pos += bufferSize) {
            System.arraycopy(data, pos, buffer, 0, Math.min(bufferSize, data.length - pos));
            queueCmd(PacketUtils.constructPacket(PacketUtils.BLETRFCmd.BLETRF_CMD_WRITE,
                    PacketUtils.BLEValues.BLEVAL_FOP_DATA,
                    buffer), progressCallback);
        }
        queueCmd(PacketUtils.constructWriteStringPacket(
                PacketUtils.BLEValues.BLEVAL_FOP_OPENFILE,
                ""));
    }

    public void queueFWUpgrade(byte[] data, BluetoothService.Cmd.Callback cleanCallback,
                               BluetoothService.Cmd.Callback progressCallback) {
        int bufferSize = 7936;

        queueCmd(PacketUtils.construstWriteIntPacket(
                PacketUtils.BLEValues.BLEVAL_CMD,
                PacketUtils.BLECmd.BLECMD_BEGIN_OTA.ordinal()), cleanCallback);

        byte[] buffer = new byte[bufferSize];
        for (int pos = 0; pos < data.length; pos += bufferSize) {
            System.arraycopy(data, pos, buffer, 0, Math.min(bufferSize, data.length - pos));
            queueCmd(PacketUtils.constructPacket(PacketUtils.BLETRFCmd.BLETRF_CMD_WRITE,
                    PacketUtils.BLEValues.BLEVAL_OTA_WRITE,
                    buffer), progressCallback);
        }

        queueCmd(PacketUtils.construstWriteIntPacket(
                PacketUtils.BLEValues.BLEVAL_CMD,
                PacketUtils.BLECmd.BLECMD_END_OTA.ordinal()), cleanCallback);

        queueCmd(PacketUtils.construstWriteIntPacket(
                PacketUtils.BLEValues.BLEVAL_CMD,
                PacketUtils.BLECmd.BLECMD_RESTART.ordinal()), cleanCallback);
    }

    private void queueGifUploadWithProgress(String remoteName, final byte[] data) {
        BluetoothService.Cmd.Callback progressCallback = new BluetoothService.Cmd.Callback() {
            @Override
            public void onProcessed(final BluetoothService.Cmd cmd) {
                mProgressBar.post(new Runnable() {
                    @Override
                    public void run() {
                        mProgressBar.setProgress(
                                mProgressBar.getProgress() + cmd.payload.length - 5);
                    }
                });
            }

            @Override
            public void onError(BluetoothService.Cmd cmd, Exception e) {

            }
        };
        BluetoothService.Cmd.Callback cleanCallback = new BluetoothService.Cmd.Callback() {
            @Override
            public void onProcessed(final BluetoothService.Cmd cmd) {
                mProgressBar.post(new Runnable() {
                    @Override
                    public void run() {
                        mProgressBar.setProgress(0);
                        mProgressBar.setMax(data.length);
                    }
                });
            }

            @Override
            public void onError(BluetoothService.Cmd cmd, Exception e) {

            }
        };
        queueGifUpload(remoteName, data, cleanCallback, progressCallback);
    }

    private void queueFWUpgradeWithProgress(final byte[] data) {
        BluetoothService.Cmd.Callback progressCallback = new BluetoothService.Cmd.Callback() {
            @Override
            public void onProcessed(final BluetoothService.Cmd cmd) {
                mProgressBar.post(new Runnable() {
                    @Override
                    public void run() {
                        mProgressBar.setProgress(
                                mProgressBar.getProgress() + cmd.payload.length - 5);
                    }
                });
            }

            @Override
            public void onError(BluetoothService.Cmd cmd, Exception e) {

            }
        };
        BluetoothService.Cmd.Callback cleanCallback = new BluetoothService.Cmd.Callback() {
            @Override
            public void onProcessed(final BluetoothService.Cmd cmd) {
                mProgressBar.post(new Runnable() {
                    @Override
                    public void run() {
                        mProgressBar.setProgress(0);
                        mProgressBar.setMax(data.length);
                    }
                });
            }

            @Override
            public void onError(BluetoothService.Cmd cmd, Exception e) {

            }
        };
        queueFWUpgrade(data, cleanCallback, progressCallback);
    }

    class CmdUICallbackAdapter implements BluetoothService.Cmd.Callback {

        @Override
        public void onProcessed(final BluetoothService.Cmd cmd) {
            getActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    onProcessedUI(cmd);
                }
            });
        }

        @Override
        public void onError(final BluetoothService.Cmd cmd, final Exception e) {
            getActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    onErrorUI(cmd, e);
                }
            });
        }

        public void onProcessedUI(BluetoothService.Cmd cmd) {
        }

        public void onErrorUI(BluetoothService.Cmd cmd, Exception e) {
        }
    }

    byte[] popupFileData;
    private PopupMenu.OnMenuItemClickListener gifPopupListener = new PopupMenu.OnMenuItemClickListener() {
        @Override
        public boolean onMenuItemClick(MenuItem menuItem) {
            switch (menuItem.getItemId()) {
                case R.id.play_once: {
                    byte[] data = popupFileData;
                    String remoteName = "10.gif";
                    queueCmd(PacketUtils.construstWriteIntPacket(
                            PacketUtils.BLEValues.BLEVAL_MODE,
                            PacketUtils.DisplayMode.DISPMODE_ANIMATION.ordinal()));
                    queueGifUploadWithProgress(remoteName, data);
                    queueCmd(PacketUtils.construstWriteIntPacket(
                            PacketUtils.BLEValues.BLEVAL_MODE,
                            PacketUtils.DisplayMode.DISPMODE_PLAY_TEMPORARY.ordinal()));
                    queueCmd(PacketUtils.constructWriteStringPacket(
                            PacketUtils.BLEValues.BLEVAL_FILENAME,
                            remoteName));

                    return true;
                }
                case R.id.set_persistent: {
                    byte[] data = popupFileData;
                    String remoteName = "9.gif";
                    queueCmd(PacketUtils.construstWriteIntPacket(
                            PacketUtils.BLEValues.BLEVAL_MODE,
                            PacketUtils.DisplayMode.DISPMODE_ANIMATION.ordinal()));
                    queueGifUploadWithProgress(remoteName, data);
                    queueCmd(PacketUtils.construstWriteIntPacket(
                            PacketUtils.BLEValues.BLEVAL_MODE,
                            PacketUtils.DisplayMode.DISPMODE_PLAY_TEMPORARY.ordinal()));
                    queueCmd(PacketUtils.constructWriteStringPacket(
                            PacketUtils.BLEValues.BLEVAL_DEFAULTFILE,
                            remoteName));
                    queueCmd(PacketUtils.constructWriteStringPacket(
                            PacketUtils.BLEValues.BLEVAL_FILENAME,
                            remoteName));

                    return true;
                }
                case R.id.add_to_grid: {
                    GifImageView gv = mGifGridItems[mGridPlacerCounter];

                    gv.setTag(popupFileData);
                    gv.setBytes(popupFileData);
                    gv.startAnimation();

                    mGridPlacerCounter++;
                    if (mGridPlacerCounter >= IMAGE_COUNT) {
                        mGridPlacerCounter = 0;
                    }
                    return true;
                }
            }
            return false;
        }
    };

    private void setupChat() {
        Log.d(TAG, "setupChat()");

        // Initialize the BluetoothService to perform bluetooth connections
        mCmdService = new BluetoothService(getActivity(), mHandler);

        /* Request current parameters */
        queueCmd(PacketUtils.construstReadPacket(PacketUtils.BLEValues.BLEVAL_BRIGHTNESS), new CmdUICallbackAdapter() {
            @Override
            public void onProcessedUI(final BluetoothService.Cmd cmd) {
                if (cmd.response != null) {
                    mBrightnessSeekBar.setProgress(PacketUtils.intFromByteArray(cmd.response));
                }
            }
        });
        queueCmd(PacketUtils.construstReadPacket(PacketUtils.BLEValues.BLEVAL_GAMMA), new CmdUICallbackAdapter() {
            @Override
            public void onProcessedUI(final BluetoothService.Cmd cmd) {
                if (cmd.response != null) {
                    mGammaSeekBar.setProgress(PacketUtils.intFromByteArray(cmd.response));
                }
            }
        });

        queueCmd(PacketUtils.construstReadPacket(PacketUtils.BLEValues.BLEVAL_FW_VER), new CmdUICallbackAdapter() {
            @Override
            public void onProcessedUI(final BluetoothService.Cmd cmd) {
                try {
                    mFWTextView.setText(new String(cmd.response, "UTF-8"));
                } catch (UnsupportedEncodingException e) {
                }
            }

            @Override
            public void onErrorUI(final BluetoothService.Cmd cmd, final Exception e) {
                mFWTextView.setText(e.getMessage());
            }
        });

        mBrightnessSeekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int i, boolean b) {
                queueCmd(PacketUtils.construstWriteIntPacket(
                        PacketUtils.BLEValues.BLEVAL_BRIGHTNESS,
                        i));
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });

        mGammaSeekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int i, boolean b) {
                queueCmd(PacketUtils.construstWriteIntPacket(
                        PacketUtils.BLEValues.BLEVAL_GAMMA,
                        i));
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });

        mModeLogoButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                queueCmd(PacketUtils.construstWriteIntPacket(
                        PacketUtils.BLEValues.BLEVAL_MODE,
                        PacketUtils.DisplayMode.DISPMODE_ANIMATION.ordinal()));
            }
        });
        mModeAnimButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                queueCmd(PacketUtils.construstWriteIntPacket(
                        PacketUtils.BLEValues.BLEVAL_MODE,
                        PacketUtils.DisplayMode.DISPMODE_PLAY_TEMPORARY.ordinal()));
            }
        });
        mModeFillButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                queueCmd(PacketUtils.construstWriteIntPacket(
                        PacketUtils.BLEValues.BLEVAL_MODE,
                        PacketUtils.DisplayMode.DISPMODE_FGFILL.ordinal()));
            }
        });

        try {
            String dir = "gifs";
            String[] files = getActivity().getAssets().list(dir);
            for (String file : files) {
                String path = dir + "/" + file;
                Log.i(TAG, "File: " + path);
                AssetFileDescriptor fd = getActivity().getAssets().openFd(path);
                final byte[] data = new byte[(int) fd.getLength()];
                DataInputStream dis = new DataInputStream(fd.createInputStream());
                dis.readFully(data);

                GifImageView gv = new GifImageView(getContext());
                gv.setTag(data);
                gv.setBytes(data);
                gv.startAnimation();
                gv.setMinimumWidth(128);
                gv.setMinimumHeight(128);

                gv.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(final View view) {
                        popupFileData = data;
                        PopupMenu popup = new PopupMenu(getActivity(), view);
                        popup.setOnMenuItemClickListener(gifPopupListener);
                        popup.inflate(R.menu.gif_popup);
                        popup.show();

                    }
                });

                mGifsList.addView(gv);
            }

        } catch (IOException e) {
            e.printStackTrace();
        }

        for (int i = -0; i < IMAGE_COUNT; i++) {
            try {
                GifImageView gv = new GifImageView(getContext());
                gv.setBackgroundColor(Color.BLUE);
                gv.setMinimumWidth(128);
                gv.setMinimumHeight(128);

                final String remoteName = i + ".gif";
                gv.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(final View view) {
                        queueCmd(PacketUtils.construstWriteIntPacket(
                                PacketUtils.BLEValues.BLEVAL_MODE,
                                PacketUtils.DisplayMode.DISPMODE_PLAY_TEMPORARY.ordinal()));
                        queueCmd(PacketUtils.constructWriteStringPacket(
                                PacketUtils.BLEValues.BLEVAL_FILENAME,
                                remoteName));
                    }
                });
                mGifGridItems[i] = gv;
                mGifGrid.addView(gv, new GridLayout.LayoutParams(GridLayout.spec(i / 3), GridLayout.spec(i % 3)));
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        try {
            Random rnd = new Random();
            String dir = "gifs";
            String[] files = getActivity().getAssets().list(dir);
            for (int i = -0; i < IMAGE_COUNT; i++) {
                String file = files[rnd.nextInt(files.length)];
                GifImageView gv = mGifGridItems[i];
                if (gv == null) continue;

                String path = dir + "/" + file;
                Log.i(TAG, "File: " + path);
                AssetFileDescriptor fd = getActivity().getAssets().openFd(path);
                final byte[] data = new byte[(int) fd.getLength()];
                DataInputStream dis = new DataInputStream(fd.createInputStream());
                dis.readFully(data);

                gv.setTag(data);
                gv.setBytes(data);
                gv.startAnimation();
            }

        } catch (IOException e) {
            e.printStackTrace();
        }

        mUploadAllButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                boolean first = true;
                int totalSize = 0;
                for (int i = 0; i < IMAGE_COUNT; i++) {
                    GifImageView gv = mGifGridItems[i];
                    if (gv == null) continue;
                    final byte[] data = (byte[]) gv.getTag();
                    if (data == null) continue;

                    totalSize += data.length;
                }
                final int maxProgress = totalSize;

                queueCmd(PacketUtils.construstWriteIntPacket(
                        PacketUtils.BLEValues.BLEVAL_MODE,
                        PacketUtils.DisplayMode.DISPMODE_ANIMATION.ordinal()));
                for (int i = 0; i < IMAGE_COUNT; i++) {
                    GifImageView gv = mGifGridItems[i];
                    if (gv == null) continue;
                    final byte[] data = (byte[]) gv.getTag();
                    if (data == null) continue;

                    String remoteName = i + ".gif";

                    BluetoothService.Cmd.Callback progressCallback = new BluetoothService.Cmd.Callback() {
                        @Override
                        public void onProcessed(final BluetoothService.Cmd cmd) {
                            mProgressBar.post(new Runnable() {
                                @Override
                                public void run() {
                                    mProgressBar.setProgress(
                                            mProgressBar.getProgress() + cmd.payload.length - 5);
                                }
                            });
                        }

                        @Override
                        public void onError(BluetoothService.Cmd cmd, Exception e) {

                        }
                    };
                    if (first) {
                        // Reset progress after starting upload of the first gif
                        BluetoothService.Cmd.Callback cleanCallback = new BluetoothService.Cmd.Callback() {
                            @Override
                            public void onProcessed(final BluetoothService.Cmd cmd) {
                                mProgressBar.post(new Runnable() {
                                    @Override
                                    public void run() {
                                        mProgressBar.setProgress(0);
                                        mProgressBar.setMax(maxProgress);
                                    }
                                });
                            }

                            @Override
                            public void onError(BluetoothService.Cmd cmd, Exception e) {

                            }
                        };
                        queueGifUpload(remoteName, data, cleanCallback, progressCallback);
                        first = false;
                    } else {
                        queueGifUpload(remoteName, data, null, progressCallback);
                    }
                }
            }
        });

        mUploadFWButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
                intent.addCategory(Intent.CATEGORY_OPENABLE);
                intent.setType("*/*");
                startActivityForResult(intent, ACTIVITY_CHOOSE_FILE);
            }
        });
        /*
         mFWTextView;
         ;
         mBrightnessSetBar;
         ;
         mGammaSetBar;
         mModeLogoButton;
         mModeAnimButton;
         mGifsList;
        * */
    }

    private void processPacket(byte[] packet) {
        try {
            PacketUtils.BLETRFResult res = PacketUtils.BLETRFResult.values()[packet[0]];
            if (res == BLETRF_RESULT_HAS_DATA) {
                //((packet[1] & 0xFF) << 8) | (packet[2] & 0xFF);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public BluetoothService.Cmd queueCmd(byte[] buffer) {
        return mCmdService.queueCmd(buffer);
    }

    public BluetoothService.Cmd queueCmd(byte[] buffer, BluetoothService.Cmd.Callback handler) {
        return mCmdService.queueCmd(buffer, handler);
    }

    /**
     * Updates the status on the action bar.
     *
     * @param resId a string resource ID
     */
    private void setStatus(int resId) {
        AppCompatActivity activity = (AppCompatActivity) getActivity();
        if (null == activity) {
            return;
        }
        final android.support.v7.app.ActionBar actionBar = activity.getSupportActionBar();
        if (null == actionBar) {
            return;
        }
        actionBar.setSubtitle(resId);
    }

    /**
     * Updates the status on the action bar.
     *
     * @param subTitle status
     */
    private void setStatus(CharSequence subTitle) {
        AppCompatActivity activity = (AppCompatActivity) getActivity();
        if (null == activity) {
            return;
        }
        final android.support.v7.app.ActionBar actionBar = activity.getSupportActionBar();
        if (null == actionBar) {
            return;
        }
        actionBar.setSubtitle(subTitle);
    }

    /**
     * The Callback that gets information back from the BluetoothService
     */
    private final Handler mHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            FragmentActivity activity = getActivity();
            switch (msg.what) {
                case Constants.MESSAGE_STATE_CHANGE:
                    switch (msg.arg1) {
                        case BluetoothService.STATE_CONNECTED:
                            setStatus(getString(R.string.title_connected_to, mConnectedDeviceName));
                            break;
                        case BluetoothService.STATE_CONNECTING:
                            setStatus(R.string.title_connecting);
                            break;
                        case BluetoothService.STATE_LISTEN:
                        case BluetoothService.STATE_NONE:
                            setStatus(R.string.title_not_connected);
                            break;
                    }
                    break;
                case Constants.MESSAGE_DEVICE_NAME:
                    // save the connected device's name
                    mConnectedDeviceName = msg.getData().getString(Constants.DEVICE_NAME);
                    if (null != activity) {
                        Toast.makeText(activity, "Connected to "
                                + mConnectedDeviceName, Toast.LENGTH_SHORT).show();
                    }
                    break;
                case Constants.MESSAGE_TOAST:
                    if (null != activity) {
                        Toast.makeText(activity, msg.getData().getString(Constants.TOAST),
                                Toast.LENGTH_SHORT).show();
                    }
                    break;
            }
        }
    };

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
            case REQUEST_CONNECT_DEVICE_SECURE:
                // When DeviceListActivity returns with a device to connect
                if (resultCode == RESULT_OK) {
                    connectDevice(data, true);
                }
                break;
            case REQUEST_CONNECT_DEVICE_INSECURE:
                // When DeviceListActivity returns with a device to connect
                if (resultCode == RESULT_OK) {
                    connectDevice(data, false);
                }
                break;
            case REQUEST_ENABLE_BT:
                // When the request to enable Bluetooth returns
                if (resultCode == RESULT_OK) {
                    // Bluetooth is now enabled, so set up a chat session
                    setupChat();
                } else {
                    // User did not enable Bluetooth or an error occurred
                    Log.d(TAG, "BT not enabled");
                    Toast.makeText(getActivity(), R.string.bt_not_enabled_leaving,
                            Toast.LENGTH_SHORT).show();
                    getActivity().finish();
                }
                break;
            case ACTIVITY_CHOOSE_FILE:
                if (resultCode == RESULT_OK) {
                    try {
                        Uri uri = data.getData();
                        InputStream inputStream = getActivity().getContentResolver().openInputStream(uri);

                        ByteArrayOutputStream byteBuffer = new ByteArrayOutputStream();
                        byte[] buffer = new byte[1024];
                        int len = 0;
                        while ((len = inputStream.read(buffer)) != -1) {
                            byteBuffer.write(buffer, 0, len);
                        }
                        byte[] fwContent = byteBuffer.toByteArray();

                        Toast.makeText(getActivity(), "Upgrading FW: " + uri.getPath(), Toast.LENGTH_LONG).show();
                        queueFWUpgradeWithProgress(fwContent);
                    } catch (IOException e) {
                        e.printStackTrace();
                        Toast.makeText(getActivity(), "Upgrade failed: " + e.getMessage(), Toast.LENGTH_LONG).show();
                    }
                }
                break;
        }
    }

    /**
     * Establish connection with other device
     *
     * @param data   An {@link Intent} with {@link DeviceListActivity#EXTRA_DEVICE_ADDRESS} extra.
     * @param secure Socket Security type - Secure (true) , Insecure (false)
     */
    private void connectDevice(Intent data, boolean secure) {
        // Get the device MAC address
        String address = data.getExtras()
                .getString(DeviceListActivity.EXTRA_DEVICE_ADDRESS);
        Log.i("BluetoothFragment", "Connecting to device " + address);
        // Get the BluetoothDevice object
        BluetoothDevice device = mBluetoothAdapter.getRemoteDevice(address);
        // Attempt to connect to the device
        mCmdService.connect(device, secure);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.bluetooth_chat, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.secure_connect_scan: {
                // Launch the DeviceListActivity to see devices and do scan
                Intent serverIntent = new Intent(getActivity(), DeviceListActivity.class);
                startActivityForResult(serverIntent, REQUEST_CONNECT_DEVICE_SECURE);
                return true;
            }
            case R.id.insecure_connect_scan: {
                // Launch the DeviceListActivity to see devices and do scan
                Intent serverIntent = new Intent(getActivity(), DeviceListActivity.class);
                startActivityForResult(serverIntent, REQUEST_CONNECT_DEVICE_INSECURE);
                return true;
            }
        }
        return false;
    }

}
